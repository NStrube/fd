use clap::Parser;
use regex_lite::Regex;
use std::path::PathBuf;
use ignore::{WalkBuilder, DirEntry};

fn main() {
    let cwd = PathBuf::from(".");
    let opts = Options::parse();
    let mut walk = WalkBuilder::new(opts.search.get(0).unwrap_or(&cwd));
    if opts.search.len() > 1 {
        for p in &opts.search[1..] {
            walk.add(p);
        }
    }
    walk.max_depth(opts.maxdepth);
    walk.hidden(opts.hidden);
    walk.follow_links(opts.follow_links);
    let walk = walk.build();

    for e in walk {
        match e {
            Ok(e) if show(&opts, &e) => println!("{}", e.path().display()),
            Ok(_) => {},
            Err(e) => eprintln!("{}", e),
        }
    }
}

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Options {
    /// The directory to search in.
    search: Vec<PathBuf>,
    /// Maximum depth to search to.
    #[arg(short = 'M', long)]
    maxdepth: Option<usize>,
    /// Minimum depth to start search from.
    #[arg(short = 'm', long)]
    mindepth: Option<usize>,
    /// Show hidden files.
    #[arg(short = 'H', long, default_value = "true")]
    hidden: bool,
    #[arg(short = 'L', long, default_value = "false")]
    follow_links: bool,
    /// Search for file names.
    #[arg(short, long = "name")]
    names: Vec<Regex>,
    /// Search for file types.
    #[arg(short = 't', long = "type")]
    filetypes: Vec<FileType>,
}

#[derive(Clone, Copy, Debug)]
enum FileType {
    Regular,
    Directory,
    Symlink,
}

impl From<&str> for FileType {
    fn from(s: &str) -> Self {
        match s {
            "f" => Self::Regular,
            "d" => Self::Directory,
            "l" => Self::Symlink,
            _ => panic!("Unknown filetype '{}'", s),
        }
    }
}

fn show(opts: &Options, e: &DirEntry) -> bool {
    opts.mindepth.map(|m| m <= e.depth()).unwrap_or(true)
        && {
            opts.names.is_empty()
                || opts.names.iter().any(|n| {
                    let file_name = e.file_name().to_str().expect("Valid UTF-8 file name");
                    n.is_match(file_name)
                })
        }
        && {
            opts.filetypes.is_empty()
                || {
                    let ft = e.file_type().unwrap();
                    opts.filetypes.iter().any(|t| {
                        match t {
                            FileType::Regular => ft.is_file(),
                            FileType::Directory => ft.is_dir(),
                            FileType::Symlink => ft.is_symlink(),
                        }
                    })}
        }
}
