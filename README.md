# fd

A simple find utility.

## Usage

~~~sh
$ # Search for names 'main.rs', 'src', 'target'
$ # Search for filetype 'directory'
$ # Exclude 'target'
$ fd -n main.rs -n src -n target -t d -e target
~~~